<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name></name>
    <message id="ofvpn-type_name">
        <source>OFVPN</source>
        <translation>OFVPN</translation>
    </message>
    <message id="ofvpn-type_description">
        <source>Full featured implementation of the Fortinet PPP+SSL VPN solution</source>
        <translation>Full featured implementation of the Fortinet PPP+SSL VPN solution</translation>
    </message>
    <message id="ofvpn-new_title">
        <source>Add new OFVPN connection</source>
        <oldsource>Add new openfortivpn connection</oldsource>
        <translation>Add new OFVPN connection</translation>
    </message>
    <message id="ofvpn-edit_title">
        <source>Edit OFVPN connection</source>
        <oldsource>Edit openfortivpn connection</oldsource>
        <translation>Edit OFVPN connection</translation>
    </message>
    <message id="ofvpn-port_error">
        <source>Port must be a value between 1 and 65535</source>
        <oldsource>Port number must be a value between 1 and 65535</oldsource>
        <translation>Port number must be a value between 1 and 65535</translation>
    </message>
    <message id="ofvpn-server_port">
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message id="ofvpn-authentication">
        <source>Authentication</source>
        <translation>Authentication</translation>
    </message>
    <message id="ofvpn-user_name">
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message id="ofvpn-two_factor_authentication">
        <source>Two-factor authentication</source>
        <translation>Two-factor authentication</translation>
    </message>
    <message id="ofvpn-connection">
        <source>Connection</source>
        <translation>Connection</translation>
    </message>
    <message id="ofvpn-set_dns">
        <source>Add VPN nameservers in /etc/resolv.conf when tunnel is up</source>
        <translation>Add VPN nameservers in /etc/resolv.conf when tunnel is up</translation>
    </message>
    <message id="ofvpn-try_to_configure_ip_routes">
        <source>Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up</source>
        <translation>Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up</translation>
    </message>
    <message id="ofvpn-half_internet_routs">
        <source>Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority</source>
        <translation>Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority</translation>
    </message>
    <message id="ofvpn-insecure_ssl">
        <source>Do not disable insecure SSL protocols/ciphers</source>
        <translation>Do not disable insecure SSL protocols/ciphers</translation>
    </message>
    <message id="ofvpn-certificates">
        <source>Certificates</source>
        <translation>Certificates</translation>
    </message>
    <message id="ofvpn-skip_certificate_check">
        <source>Skip certificate check</source>
        <translation>Skip certificate check</translation>
    </message>
    <message id="ofvpn-allow_self_signed_certificate">
        <source>Allow self signed certificate</source>
        <translation>Allow self signed certificate</translation>
    </message>
    <message id="ofvpn-trusted_cert">
        <source>Trusted certificate</source>
        <translation>Trusted certificate</translation>
    </message>
    <message id="ofvpn-user_certificate">
        <source>User certificate</source>
        <translation>User certificate</translation>
    </message>
    <message id="ofvpn-user_key">
        <source>User key</source>
        <translation>User key</translation>
    </message>
    <message id="ofvpn-ca_file">
        <source>CA file</source>
        <translation>CA file</translation>
    </message>
</context>
</TS>
