<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name></name>
    <message id="ofvpn-type_name">
        <source>OFVPN</source>
        <translation>OFVPN</translation>
    </message>
    <message id="ofvpn-type_description">
        <source>Full featured implementation of the Fortinet PPP+SSL VPN solution</source>
        <translation>Полнофункциональная реализация решения Fortinet PPP+SSL VPN</translation>
    </message>
    <message id="ofvpn-new_title">
        <source>Add new OFVPN connection</source>
        <oldsource>Add new openfortivpn connection</oldsource>
        <translation>Добавить новое соединение OFVPN</translation>
    </message>
    <message id="ofvpn-edit_title">
        <source>Edit OFVPN connection</source>
        <oldsource>Edit openfortivpn connection</oldsource>
        <translation>Редактировать соединение OFVPN</translation>
    </message>
    <message id="ofvpn-port_error">
        <source>Port must be a value between 1 and 65535</source>
        <oldsource>Port number must be a value between 1 and 65535</oldsource>
        <translation>Номер порта должен быть значением от 1 до 65535</translation>
    </message>
    <message id="ofvpn-server_port">
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message id="ofvpn-authentication">
        <source>Authentication</source>
        <translation>Аутентификация</translation>
    </message>
    <message id="ofvpn-user_name">
        <source>Username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message id="ofvpn-two_factor_authentication">
        <source>Two-factor authentication</source>
        <translation>Двухфакторная аутентификация</translation>
    </message>
    <message id="ofvpn-connection">
        <source>Connection</source>
        <translation>Соединение</translation>
    </message>
    <message id="ofvpn-set_dns">
        <source>Add VPN nameservers in /etc/resolv.conf when tunnel is up</source>
        <translation>Добавить сервера имен от VPN в /etc/resolv.conf когда туннель поднят</translation>
    </message>
    <message id="ofvpn-try_to_configure_ip_routes">
        <source>Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up</source>
        <translation>Разрешить openfortivpn настраивать IP маршруты через VPN когда туннель поднят</translation>
    </message>
    <message id="ofvpn-half_internet_routs">
        <source>Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority</source>
        <translation>Добавить маршруты 0.0.0.0/1 и 128.0.0.0/1 с более высоким приоритетом</translation>
    </message>
    <message id="ofvpn-insecure_ssl">
        <source>Do not disable insecure SSL protocols/ciphers</source>
        <translation>Не отключать небезопасные протоколы/шифры SSL</translation>
    </message>
    <message id="ofvpn-certificates">
        <source>Certificates</source>
        <translation>Сертификаты</translation>
    </message>
    <message id="ofvpn-skip_certificate_check">
        <source>Skip certificate check</source>
        <translation>Пропустить проверку сертификата</translation>
    </message>
    <message id="ofvpn-allow_self_signed_certificate">
        <source>Allow self signed certificate</source>
        <translation>Разрешить самоподписанный сертификат</translation>
    </message>
    <message id="ofvpn-trusted_cert">
        <source>Trusted certificate</source>
        <translation>Доверенный сертификат</translation>
    </message>
    <message id="ofvpn-user_certificate">
        <source>User certificate</source>
        <translation>Сертификат пользователя</translation>
    </message>
    <message id="ofvpn-user_key">
        <source>User key</source>
        <translation>Ключ пользователя</translation>
    </message>
    <message id="ofvpn-ca_file">
        <source>CA file</source>
        <translation>Файл CA</translation>
    </message>
</context>
</TS>
