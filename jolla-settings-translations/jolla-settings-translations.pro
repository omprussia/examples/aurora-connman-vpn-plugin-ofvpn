# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = aux

TRANSLATIONS = ofvpn-networking-vpn-plugin.ts \ # Default translation
               ofvpn-networking-vpn-plugin-ru.ts \
               ofvpn-networking-vpn-plugin-en_US.ts

for(t, TRANSLATIONS) {
    TRANSLATIONS_IN += $${_PRO_FILE_PWD_}/$$t
}

qm.files = $$replace(TRANSLATIONS_IN, \.ts, .qm)
qm.path = $$LIBDIR/qt5/qml/ru/omprussia/ofvpn/translations
qm.CONFIG += no_check_exist
qm.commands += lupdate -no-obsolete -locations none $$PWD/.. -ts $$TRANSLATIONS_IN \
               && lrelease -idbased $$TRANSLATIONS_IN

QMAKE_EXTRA_TARGETS += qm
INSTALLS += qm
PRE_TARGETDEPS += qm

QMAKE_CLEAN += "$$PWD/*.qm"
