# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS = \
    connman-plugin \
    jolla-settings-controls \
    jolla-settings-translations \
    qml-plugin \
    aurora-connman-vpn-plugin-ofvpn

OTHER_FILES += \
    README.md \
    README.ru.md \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-Clause.md \
    rpm/ru.auroraos.aurora-connman-vpn-plugin-ofvpn.spec
