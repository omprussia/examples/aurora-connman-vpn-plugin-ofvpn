﻿// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <glib-2.0/glib.h>
#include <dbus-1.0/dbus/dbus.h>
#include <errno.h>
#include <linux/if_tun.h>
#include <sys/socket.h>

#include <connman/plugin.h> // Connman plugin registration
#include <connman/task.h> // Connman binary execution
#include <connman/log.h> // Connman logging functions

#include <connman/dbus.h>
#include <connman/vpn/plugins/vpn.h>
#include <connman/vpn-dbus.h> // VPN_AGENT_INTERFACE

#include <connman/agent.h>
#include <connman/ipaddress.h>
#include <connman/setting.h>
#include <connman/vpn/vpn-agent.h>

#define PLUGIN_NAME "ofvpn"
#define BIN_PATH "/usr/bin/openfortivpn"
#define SCRIPTDIR "/usr/lib/connman/scripts"
#define FNV_PORT "10443"
static DBusConnection *connection;

#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

/*!
 * \brief Parameter type.
 */
enum opt_type {
    OPT_STRING	= 0,
    OPT_BOOL	= 1,
};

/*!
 * \brief Defines the parameters used by the OFVPN.
 */
struct {
    const char	*cm_opt;
    const char	*fnv_opt;
    bool		has_value;
    bool		enabled;
    enum opt_type	type;
} fnv_options[] = {
    { "ofvpn.AllowSelfSignedCert", "--trust-all-certs", 0, 1, OPT_BOOL},
    { "ofvpn.TrustedCert", "--trusted-cert", 1, 1, OPT_STRING},
    { "ofvpn.CertSkipCheck", "--cert-skip-check", 0, 1, OPT_BOOL},
    { "ofvpn.Port", NULL, 1, 0, OPT_STRING},
    { "ofvpn.SetRoutes", "--set-routes", 1, 1, OPT_BOOL},
    { "ofvpn.SetDNS", "--set-dns", 1, 1, OPT_BOOL},
    { "ofvpn.HalfInternetRoutes", "--half-internet-routes", 1, 1, OPT_BOOL},
    { "ofvpn.InsecureSSL", "--insecure-ssl", 0, 1, OPT_BOOL},
    { "ofvpn.OneTimePassword", "--otp", 1, 1, OPT_STRING},
    { "ofvpn.AskOTP", NULL, 1, 0, OPT_BOOL},
    { "ofvpn.UserCert", "--user-cert", 1, 1, OPT_STRING},
    { "ofvpn.UserKey", "--user-key", 1, 1, OPT_STRING},
    { "ofvpn.CAfile", "--ca-file", 1, 1, OPT_STRING}
};

/*!
 * \brief Provider's data.
 */
struct fnv_private_data {
    struct connman_task *task;
    char *if_name;
    vpn_provider_connect_cb_t cb;
    void *user_data;
};

/*!
 * \brief Defines the provider's notification function.
 */
static DBusMessage *fnv_get_sec(struct connman_task *task G_GNUC_UNUSED, DBusMessage *msg, void *user_data)
{
    const char *user, *passwd;
    struct vpn_provider *provider = user_data;

    if (!dbus_message_get_no_reply(msg)) {
        DBusMessage *reply;

        user = vpn_provider_get_string(provider, "ofvpn.User");
        passwd = vpn_provider_get_string(provider, "ofvpn.Password");

        if (!user || strlen(user) == 0 || !passwd ||
                strlen(passwd) == 0)
            return NULL;

        reply = dbus_message_new_method_return(msg);
        if (!reply)
            return NULL;

        dbus_message_append_args(reply, DBUS_TYPE_STRING, &user, DBUS_TYPE_STRING, &passwd,
                                 DBUS_TYPE_INVALID);
        return reply;
    }
    return NULL;
}

/*!
 * \brief Terminates the connman task.
 */
static void fnv_died(struct connman_task *task, int exit_code, void *user_data)
{
    DBG("fnv_died: task %p, code %d, data %p", task, exit_code, user_data);
    vpn_died(task, exit_code, user_data);
    return;
}

/*!
 * \brief The request_input_reply struct.
 */
struct request_input_reply {
    struct vpn_provider *provider;
    vpn_provider_password_cb_t callback;
    void *user_data;
};

/*!
 * \brief Handles additional data entry.
 */
static void request_input_reply(DBusMessage *reply, void *user_data)
{
    struct request_input_reply *fnv_reply = user_data;
    struct fnv_private_data *data = NULL;
    const char *error = NULL;
    char *username = NULL, *password = NULL, *oneTimePassword = NULL;
    char *key;
    DBusMessageIter iter, dict;
    int err;

    DBG("provider %p", fnv_reply->provider);

    if (!reply)
        goto done;

    data = fnv_reply->user_data;

    err = vpn_agent_check_and_process_reply_error(reply, fnv_reply->provider, data->task,
                                                  data->cb, data->user_data);
    if (err) {
        /* Ensure cb is called only once */
        data->cb = NULL;
        data->user_data = NULL;
        error = dbus_message_get_error_name(reply);
        goto done;
    }

    if (!vpn_agent_check_reply_has_dict(reply))
        goto done;

    dbus_message_iter_init(reply, &iter);
    dbus_message_iter_recurse(&iter, &dict);
    while (dbus_message_iter_get_arg_type(&dict) == DBUS_TYPE_DICT_ENTRY) {
        DBusMessageIter entry, value;
        const char *str;

        dbus_message_iter_recurse(&dict, &entry);
        if (dbus_message_iter_get_arg_type(&entry) != DBUS_TYPE_STRING)
            break;

        dbus_message_iter_get_basic(&entry, &key);

        if (g_str_equal(key, "Username")) {
            dbus_message_iter_next(&entry);
            if (dbus_message_iter_get_arg_type(&entry) !=
                    DBUS_TYPE_VARIANT)
                break;
            dbus_message_iter_recurse(&entry, &value);
            if (dbus_message_iter_get_arg_type(&value) !=
                    DBUS_TYPE_STRING)
                break;
            dbus_message_iter_get_basic(&value, &str);
            username = g_strdup(str);
        }

        if (g_str_equal(key, "Password")) {
            dbus_message_iter_next(&entry);
            if (dbus_message_iter_get_arg_type(&entry) !=
                    DBUS_TYPE_VARIANT)
                break;
            dbus_message_iter_recurse(&entry, &value);
            if (dbus_message_iter_get_arg_type(&value) !=
                    DBUS_TYPE_STRING)
                break;
            dbus_message_iter_get_basic(&value, &str);
            password = g_strdup(str);
        }

        if (g_str_equal(key, "OTP")) {
            dbus_message_iter_next(&entry);
            if (dbus_message_iter_get_arg_type(&entry) !=
                    DBUS_TYPE_VARIANT)
                break;
            dbus_message_iter_recurse(&entry, &value);
            if (dbus_message_iter_get_arg_type(&value) !=
                    DBUS_TYPE_STRING)
                break;
            dbus_message_iter_get_basic(&value, &str);
            oneTimePassword = g_strdup(str);
            vpn_provider_set_string(fnv_reply->provider, "ofvpn.OneTimePassword", oneTimePassword);
        }

        dbus_message_iter_next(&dict);
    }

done:
    fnv_reply->callback(fnv_reply->provider, username, password, error, fnv_reply->user_data);

    g_free(username);
    g_free(password);
    g_free(oneTimePassword);
    g_free(fnv_reply);
}

typedef void (*request_cb_t)(struct vpn_provider *provider, const char *username, const char *password,
                             const char *error, void *user_data);

/*!
 * \brief Adds the required field to the message.
 * \param iter Message iter.
 * \param str_type Value type.
 * \param str Message identifier.
 * \param user_data Data.
 */
static void request_input_append(DBusMessageIter *iter,
        const char *str_type, const char *str, void *user_data)
{
    const char *string;

    connman_dbus_dict_append_basic(iter, "Type", DBUS_TYPE_STRING, &str_type);
    connman_dbus_dict_append_basic(iter, "Requirement", DBUS_TYPE_STRING, &str);

    if (!user_data)
        return;

    string = user_data;
    connman_dbus_dict_append_basic(iter, "Value", DBUS_TYPE_STRING, &string);
}

/*!
 * \brief Forms a request to add a field.
 */
static void request_input_append_mandatory(DBusMessageIter *iter,
        void *user_data)
{
    request_input_append(iter, "string", "mandatory", user_data);
}

/*!
 * \brief Generates a message for the additional data entry window.
 */
static int request_input(struct vpn_provider *provider, request_cb_t callback,
                         const char *dbus_sender, void *user_data)
{
    DBusMessage *message;
    const char *path, *agent_sender, *agent_path;
    bool askOTP;
    DBusMessageIter iter;
    DBusMessageIter dict;
    struct request_input_reply *fnv_reply;
    int err;
    void *agent;

    askOTP = vpn_provider_get_boolean(provider, "ofvpn.AskOTP", false);

    agent = connman_agent_get_info(dbus_sender, &agent_sender, &agent_path);
    if (!provider || !agent || !agent_path || !callback)
        return -ESRCH;

    message = dbus_message_new_method_call(agent_sender, agent_path, VPN_AGENT_INTERFACE, "RequestInput");
    if (!message)
        return -ENOMEM;

    dbus_message_iter_init_append(message, &iter);

    path = vpn_provider_get_path(provider);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &path);

    connman_dbus_dict_open(&iter, &dict);
    if (vpn_provider_get_authentication_errors(provider))
        vpn_agent_append_auth_failure(&dict, provider, NULL);

    vpn_agent_append_user_info(&dict, provider, "ofvpn.User");
    vpn_agent_append_host_and_name(&dict, provider);
    vpn_agent_append_allow_credential_retrieval(&dict, !askOTP);
    vpn_agent_append_allow_credential_storage(&dict, !askOTP);
    if (askOTP)
        connman_dbus_dict_append_dict(&dict, "OTP", request_input_append_mandatory, NULL);

    connman_dbus_dict_close(&iter, &dict);

    fnv_reply = g_try_new0(struct request_input_reply, 1);
    if (!fnv_reply) {
        dbus_message_unref(message);
        return -ENOMEM;
    }

    fnv_reply->provider = provider;
    fnv_reply->callback = callback;
    fnv_reply->user_data = user_data;

    err = connman_agent_queue_message(provider, message, connman_timeout_input_request(),
                                      request_input_reply, fnv_reply, agent);
    if (err < 0 && err != -EBUSY) {
        DBG("error %d sending agent request", err);
        dbus_message_unref(message);
        g_free(fnv_reply);
        return err;
    }
    dbus_message_unref(message);
    return -EINPROGRESS;
}

/*!
 * \brief Adds parameters and their values for use by the utility.
 */
static int task_append_config_data(struct vpn_provider *provider, struct connman_task *task)
{
    const char *value = NULL;
    int i;

    for (i = 0; i < (int)ARRAY_SIZE(fnv_options); i++) {
        if (!fnv_options[i].fnv_opt || !fnv_options[i].enabled)
            continue;

        if (fnv_options[i].has_value) {
            char *opt;

            value = vpn_provider_get_string(provider, fnv_options[i].cm_opt);
            if (!value)
                continue;
            opt = g_strconcat(fnv_options[i].fnv_opt, "=", value, NULL);
            connman_task_add_argument(task, opt, NULL);
            g_free(opt);
        }

        /* Add boolean type values only if set as true. */
        if (fnv_options[i].type == OPT_BOOL) {
            if (!vpn_provider_get_boolean(provider, fnv_options[i].cm_opt, false))
                continue;
            connman_task_add_argument(task, fnv_options[i].fnv_opt, NULL);
        }
    }
    return 0;
}

/*!
 * \brief Forms a task for execution by the utility.
 */
static int run_connect(struct vpn_provider *provider, struct connman_task *task,
                       const char *if_name G_GNUC_UNUSED, vpn_provider_connect_cb_t cb,
                       void *user_data, const char *username,
                       const char *password)
{
    char *host, *port, *gateway, *esc_user, *esc_pass, *esc_pppdPlugin;
    int err;

    if (!username || !*username || !password || !*password) {
        DBG("Cannot connect username %s password %p", username,
            password);
        err = -EINVAL;
        goto done;
    }
    DBG("username %s password %p", username, password);

    host = strdup(vpn_provider_get_string(provider, "Host"));
    port = strdup(vpn_provider_get_string(provider, "ofvpn.Port"));

    DBG("host %s port %p", host, port);

    connman_task_add_argument(task, "-vvv", NULL);

    esc_user = g_strdup_printf("--username=%s", username);
    connman_task_add_argument(task, esc_user, NULL);

    esc_pass = g_strdup_printf("--password=%s", password);
    connman_task_add_argument(task, esc_pass, NULL);

    if (!port || !*port) {
        gateway = g_strconcat(host, ":", FNV_PORT, NULL);
    } else {
        gateway = g_strconcat(host, ":", port, NULL);
    }
    connman_task_add_argument(task, gateway, NULL);

    esc_pppdPlugin = g_strconcat("--pppd-plugin=", SCRIPTDIR, "/libppp-plugin.so", NULL);
    connman_task_add_argument(task, esc_pppdPlugin, NULL);

    connman_task_add_argument(task, "--use-syslog", NULL);

    task_append_config_data(provider, task);

    g_free(gateway);
    g_free(esc_user);
    g_free(esc_pass);
    g_free(esc_pppdPlugin);

    err = connman_task_run(task, fnv_died, provider, NULL, NULL, NULL);
    if (err < 0) {
        connman_error("fnv failed to start");
        err = -EIO;
        goto done;
    }

done:
    if (cb)
        cb(provider, user_data, err);

    return err;
}

/*!
 * \brief Clears allocated memory.
 */
static void free_private_data(struct fnv_private_data *data)
{
    g_free(data->if_name);
    g_free(data);
}

/*!
 * \brief Passes additional data for execution by the utility.
 */
static void request_input_cb(struct vpn_provider *provider,
                             const char *username, const char *password,
                             const char *error, void *user_data)
{
    struct fnv_private_data *data = user_data;

    if (!username || !*username || !password || !*password)
        DBG("Requesting username %s or password failed, error %s",
            username, error);
    else if (error)
        DBG("error %s", error);

    vpn_provider_set_string(provider, "ofvpn.User", username);
    vpn_provider_set_string_hide_value(provider, "ofvpn.Password", password);

    run_connect(provider, data->task, data->if_name, data->cb,
                data->user_data, username, password);

    free_private_data(data);
}

/*!
 * \brief Processes the result of connecting to the network.
 */
static int fnv_notify(DBusMessage *msg, struct vpn_provider *provider)
{
    DBG("fnv_notify provider %p", provider);

    DBusMessageIter iter, dict;
    const char *reason, *key, *value;
    char *addressv4 = NULL, *netmask = NULL, *gateway = NULL;
    char *ifname = NULL, *nameservers = NULL;
    struct connman_ipaddress *ipaddress = NULL;

    dbus_message_iter_init(msg, &iter);

    dbus_message_iter_get_basic(&iter, &reason);
    dbus_message_iter_next(&iter);

    if (!provider) {
        connman_error("No provider found");
        return VPN_STATE_FAILURE;
    }
    if (strcmp(reason, "auth failed") == 0) {
        DBG("authentication failure");

        vpn_provider_set_string(provider, "ofvpn.User", NULL);
        vpn_provider_set_string_hide_value(provider, "ofvpn.Password", NULL);
        return VPN_STATE_AUTH_FAILURE;
    }
    if (strcmp(reason, "connect"))
        return VPN_STATE_DISCONNECT;

    dbus_message_iter_recurse(&iter, &dict);

    while (dbus_message_iter_get_arg_type(&dict) == DBUS_TYPE_DICT_ENTRY) {
        DBusMessageIter entry;

        dbus_message_iter_recurse(&dict, &entry);
        dbus_message_iter_get_basic(&entry, &key);
        dbus_message_iter_next(&entry);
        dbus_message_iter_get_basic(&entry, &value);

        DBG("%s = %s", key, value);

        if (!strcmp(key, "INTERNAL_IP4_ADDRESS"))
            addressv4 = g_strdup(value);

        if (!strcmp(key, "INTERNAL_IP4_NETMASK"))
            netmask = g_strdup(value);

        if (!strcmp(key, "INTERNAL_IP4_DNS"))
            nameservers = g_strdup(value);

        if (!strcmp(key, "INTERNAL_IFNAME"))
            ifname = g_strdup(value);

        dbus_message_iter_next(&dict);
    }

    if (vpn_set_ifname(provider, ifname) < 0) {
        g_free(ifname);
        g_free(addressv4);
        g_free(netmask);
        g_free(nameservers);
        return VPN_STATE_FAILURE;
    }
    if (addressv4)
        ipaddress = connman_ipaddress_alloc(AF_INET);

    g_free(ifname);
    if (!ipaddress) {
        connman_error("No IP address for provider");
        g_free(addressv4);
        g_free(netmask);
        g_free(nameservers);
        return VPN_STATE_FAILURE;
    }
    value = vpn_provider_get_string(provider, "HostIP");
    if (value) {
        vpn_provider_set_string(provider, "Gateway", value);
        gateway = g_strdup(value);
    }

    if (addressv4)
        connman_ipaddress_set_ipv4(ipaddress, addressv4, netmask, gateway);

    vpn_provider_set_ipaddress(provider, ipaddress);
    vpn_provider_set_nameservers(provider, nameservers);

    g_free(addressv4);
    g_free(netmask);
    g_free(gateway);
    g_free(nameservers);
    connman_ipaddress_free(ipaddress);
    return VPN_STATE_CONNECT;
}

/*!
 * \brief Generates a network connection request.
 */
static int fnv_connect(struct vpn_provider *provider, struct connman_task *task,
                       const char *if_name, vpn_provider_connect_cb_t cb,
                       const char *dbus_sender, void *user_data)
{
    DBG("fnv_connect provider %p", provider);

    const char *username, *password;
    bool askOTP;
    int err = -ENETUNREACH;

    if (connman_task_set_notify(task, "getsec", fnv_get_sec, provider) !=
            0) {
        err = -ENOMEM;
        goto error;
    }
    username = vpn_provider_get_string(provider, "ofvpn.User");
    password = vpn_provider_get_string(provider, "ofvpn.Password");
    askOTP = vpn_provider_get_boolean(provider, "ofvpn.AskOTP", false);

    DBG("user %s password %p", username, password);

    if (!username || !*username || !password || !*password || askOTP) {
        struct fnv_private_data *data;

        data = g_try_new0(struct fnv_private_data, 1);
        if (!data)
            return -ENOMEM;

        data->task = task;
        data->if_name = g_strdup(if_name);
        data->cb = cb;
        data->user_data = user_data;

        err = request_input(provider, request_input_cb, dbus_sender,
                            data);
        if (err != -EINPROGRESS) {
            free_private_data(data);
            goto done;
        }
        return err;
    }

done:
    return run_connect(provider, task, if_name, cb, user_data, username,
                       password);

error:
    if (cb)
        cb(provider, user_data, err);

    return err;
}

/*!
 * \brief Processes the result of disconnecting from the network.
 */
static void fnv_disconnect(struct vpn_provider *provider)
{
    DBG("fnv_disconnect provider %p", provider);

    if (!provider)
        return;

    /*
    * Cancelling the agent request
    * to avoid having multiple ones visible in case of timeout.
    */
    connman_agent_cancel(provider);
    vpn_provider_set_string_hide_value(provider, "ofvpn.Password", NULL);
}

/*!
 * \brief Transmits an error code when connecting to the network.
 */
static int fnv_error_code(struct vpn_provider *provider, int exit_code)
{
    DBG("provider %p exit %d", provider, exit_code);
    vpn_provider_set_string_hide_value(provider, "ofvpn.Password", NULL);
    return exit_code;
}

/*!
 * \brief Saves the network configuration before exiting the plugin.
 */
static int fnv_save(struct vpn_provider *provider, GKeyFile *keyfile)
{
    const char *save_group;
    const char *option;
    int i;

    DBG("provider %p", provider);

    save_group = vpn_provider_get_save_group(provider);

    for (i = 0; i < (int)ARRAY_SIZE(fnv_options); i++) {
        option = vpn_provider_get_string(provider, fnv_options[i].cm_opt);
        if (!option)
            continue;
        g_key_file_set_string(keyfile, save_group, fnv_options[i].cm_opt, option);
    }
    return 0;
}

/*!
 * \brief Specifies the connection flags.
 */
static int fnv_device_flags(struct vpn_provider *provider)
{
    DBG("fnv_device_flags provider %p", provider);
    return IFF_TUN;
}

/*!
 * \brief Handles connection parameters.
 */
static int fnv_route_env_parse(struct vpn_provider *provider, const char *key,
                               int *family, unsigned long *idx,
                               enum vpn_provider_route_type *type)
{
    DBG("fnv_route_env_parse provider %p", provider);

    char *end = NULL;
    const char *start;

    if (g_str_has_prefix(key, "route_network_")) {
        start = key + strlen("route_network_");
        *type = VPN_PROVIDER_ROUTE_TYPE_ADDR;
    } else if (g_str_has_prefix(key, "route_netmask_")) {
        start = key + strlen("route_netmask_");
        *type = VPN_PROVIDER_ROUTE_TYPE_MASK;
    } else if (g_str_has_prefix(key, "route_gateway_")) {
        start = key + strlen("route_gateway_");
        *type = VPN_PROVIDER_ROUTE_TYPE_GW;
    } else
        return -EINVAL;

    *family = AF_INET;
    *idx = g_ascii_strtoull(start, &end, 10);
    if (!end) {
        return errno;
    }
    return 0;
}

/*!
 * \brief Defines the plugin configuration.
 */
static struct vpn_driver vpn_driver = {
    .notify = fnv_notify,
    .connect = fnv_connect,
    .disconnect = fnv_disconnect,
    .error_code = fnv_error_code,
    .save = fnv_save,
    .device_flags = fnv_device_flags,
    .route_env_parse = fnv_route_env_parse
};

/*!
 * \brief Initializes the plugin.
 */
static int ofvpn_init(void)
{
    connection = connman_dbus_get_connection();
    return vpn_register(PLUGIN_NAME, &vpn_driver, BIN_PATH);
}

/*!
 * \brief Terminates the plugin.
 */
static void ofvpn_exit(void)
{
    vpn_unregister(PLUGIN_NAME);
    dbus_connection_unref(connection);
}

CONNMAN_PLUGIN_DEFINE(ofvpn, "OFVPN Plugin", CONNMAN_VERSION,
                      CONNMAN_PLUGIN_PRIORITY_DEFAULT, ofvpn_init,
                      ofvpn_exit)
