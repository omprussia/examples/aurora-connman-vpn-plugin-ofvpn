# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = lib
CONFIG -= qt
CONFIG += no_plugin_name_prefix
CONFIG += plugin c++11
CONFIG += link_pkgconfig
PKGCONFIG += glib-2.0 dbus-1

SOURCES += \
    src/aurora-ofvpn-vpn.c

OTHER_FILES += \
    ofvpn.conf

TARGET = ofvpn
target.path = $$LIBDIR/connman/plugins-vpn

connman_config.files = ofvpn.conf
connman_config.path = /etc/connman/vpn-plugin/

INSTALLS += target connman_config
