# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.aurora-connman-vpn-plugin-ofvpn

CONFIG += auroraapp

SOURCES += \
    src/aurora-connman-vpn-plugin-ofvpn.cpp
