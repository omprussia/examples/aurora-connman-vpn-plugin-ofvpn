%define __provides_exclude_from ^%{_libdir}/.*$

Name: ru.auroraos.aurora-connman-vpn-plugin-ofvpn
Version: 0.1
Release: 3
Summary: Aurora Connman external VPN plugin openfortivpn
License: BSD-3-Clause
URL:     https://developer.auroraos.ru/open-source
Source: %{name}-%{version}.tar.bz2
BuildRequires: pkgconfig(auroraapp)
BuildRequires: pkgconfig(connman)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(dbus-1)

%description
This package contains a openfortivpn of Aurora Connman external VPN plugin with a settings menu GUI.

# TODO: split back connman plugin and jolla-settings UI plugin

%changelog
* Thu Jan 30 2020 Alexey Andreyev <a.andreev@omprussia.ru> - 0.1-2
- Public version for the current Aurora OS
* Thu Mar 14 2019 Alexey Andreyev <a.andreev@omprussia.ru> - 0.1-1
- Initial Version

%prep
%autosetup

%build

%qmake5
%make_build

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/*

%defattr(644,root,root,-)
%{_libdir}/connman/plugins-vpn/*.so
%{_libdir}/qt5/qml/ru/omprussia/ofvpn/*
%{_sysconfdir}/connman/vpn-plugin/*.conf
%{_datadir}/sailfish-vpn/*
