# OFVPN Plugin

An example project that shows how to
integrate VPN into Aurora OS based on the openfortivpn application.

The main purpose is to demonstrate using a minimum of source code to
get a correct VPN plugin with an external configuration file.

The application signing must be made with **extended**-keys.

## Dependencies

+ connman
+ connman-vpn-scripts
+ glib-2.0
+ dbus-1
+ [openfortivpn](https://github.com/sailfishos-mirror/openfortivpn/tree/master) fork
+ jolla-settings as a gui client

## Limitations

+ CLI passing of the password, related: [NetworkManager-fortisslvpn#12](https://gitlab.gnome.org/GNOME/NetworkManager-fortisslvpn/issues/12)
+ Since openfortivpn has no async control interface (similar to OpenVPN), option skip cert check was temporary added to openfortivpn

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

* **[aurora-connman-vpn-plugin-ofvpn.pro](aurora-connman-vpn-plugin-ofvpn.pro)** file
 describes the project structure for the qmake build system.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[aurora-connman-vpn-plugin-ofvpn.spec](rpm/aurora-connman-vpn-plugin-ofvpn.spec)** file is used by rpmbuild tool.
* **[jolla-settings-controls](jolla-settings-controls)** directory contains QML settings controls files for Aurora OS 3 and Aurora OS 4.
  * **[jolla-settings-controls/aurora_4_0](jolla-settings-controls/aurora_4_0/ofvpn)** directory contains QML files for
  the *jolla-setting* sailfish-vpn module for Aurora OS 4.
  * **[jolla-settings-controls/aurora_3_5](jolla-settings-controls/aurora_3_5/ofvpn)** directory contains QML files for
  the *jolla-setting* sailfish-vpn module for Aurora OS 3.
* **[jolla-settings-translations](jolla-settings-translations)** directory contains the OFVPN plugin translation files.
* **[connman-plugin](connman-plugin)** directory contains subproject with a wrapper of the ConnMan plugin.
  * **[aurora-ofvpn-vpn.c](connman-plugin/src/aurora-ofvpn-vpn.c)** is the main file with the `CONNMAN_PLUGIN_DEFINE` macros.
  * **[clang-format](connman-plugin/src/.clang-format)** file is a clang-format configuration file from Linux kernel.
* **[qml-plugin](qml-plugin)** directory contains the implementation of the OFVPN plugin.

## Compatibility

The project is compatible with all the current versions of the Aurora OS

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
