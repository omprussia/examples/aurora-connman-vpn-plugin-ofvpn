// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "ofvpnplugin.h"
#include "ofvpntranslator.h"
#include "stubitem.h"

/*!
 * \brief Sets translations for plugin fields.
 */
void OFVPNPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_ASSERT(uri == QLatin1String("ru.omprussia.ofvpn"));
    qInfo("Was here");

    // Always load default translations first, otherwise we'll see
    // only string IDs when running jolla-settings from terminal
    OFVPNTranslator *defaultTranslator = new OFVPNTranslator(engine);
    defaultTranslator->load(QLocale(), "ofvpn-networking-vpn-plugin", "-",
                            "/usr/lib/qt5/qml/ru/omprussia/ofvpn/translations");
    qInfo("Loading first");

    OFVPNTranslator *translator = new OFVPNTranslator(engine);
    translator->load(QLocale(), "ofvpn-networking-vpn-plugin", "-",
                     "/usr/lib/qt5/qml/ru/omprussia/ofvpn/translations");
    qInfo("Loading second");
    engine->installEventFilter(translator);
    qInfo("Load completed");
}

/*!
 * \brief Registers new types.
 */
void OFVPNPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("ru.omprussia.ofvpn"));
    qInfo("Register types");
    qmlRegisterType<StubItem>("ru.omprussia.ofvpn", 1, 0, "StubItem");
}
