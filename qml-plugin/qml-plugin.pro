# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = lib
TARGET = ofvpnplugin
QT += qml
QT -= gui
CONFIG += plugin c++14

HEADERS += \
    ofvpnplugin.h \
    ofvpntranslator.h \
    stubitem.h

SOURCES += \
    ofvpnplugin.cpp \
    ofvpntranslator.cpp \
    stubitem.cpp

OTHER_FILES += qmldir

MODULENAME = ru/omprussia/ofvpn
TARGETPATH = $$LIBDIR/qt5/qml/$$MODULENAME

import.files = qmldir
import.path = $$TARGETPATH
target.path = $$TARGETPATH

INSTALLS += target import
