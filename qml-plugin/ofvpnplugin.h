// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef OFVPNPLUGIN_H
#define OFVPNPLUGIN_H

#include <QtQml>

/*!
 * \brief The OFVPNPlugin class
 */
class OFVPNPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "ru.omprussia.ofvpn")

public:
    void initializeEngine(QQmlEngine *engine, const char *uri) override;
    void registerTypes(const char *uri) override;
};

#endif // OFVPNPLUGIN_H
