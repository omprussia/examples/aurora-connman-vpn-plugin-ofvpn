// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "ofvpntranslator.h"

/*!
 * \brief Installs a translator for the application.
 */
OFVPNTranslator::OFVPNTranslator(QObject *parent) : QTranslator(parent)
{
    qApp->installTranslator(this);
}

/*!
 * \brief Removes the translator of the application.
 */
OFVPNTranslator::~OFVPNTranslator()
{
    qApp->removeTranslator(this);
}

/*!
 * \brief Loads the content of translations.
 */
bool OFVPNTranslator::eventFilter(QObject *watched, QEvent *event)
{
    qInfo("event?");
    if (watched == this && event->type() == QEvent::LanguageChange) {
        bool loaded = load(QLocale(), "ofvpn-networking-vpn-plugin", "-",
                           "/usr/lib/qt5/qml/ru/omprussia/ofvpn/translations");
        qInfo() << "loaded?" << loaded;
    }

    return QTranslator::eventFilter(watched, event);
}
