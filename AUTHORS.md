# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2021-2022
* Alexey Andreev <a.andreev@omp.ru>
  * Reviewer, 2022
  * Maintainer, 2022-2023
* Andrey Vasilyev
  * Reviewer, 2021
* Nikita Medvedev
  * Developer, 2021
* Andrey Tretyakov, <a.tretyakov@omp.ru>
  * Reviewer, 2023
* Vladislav Larionov
  * Reviewer, 2023
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Reviewer, 2023
  * Maintainer, 2023
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
