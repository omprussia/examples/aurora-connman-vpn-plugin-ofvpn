# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = aux
QT -= gui

OTHER_FILES += \
    aurora_3_5/ofvpn/*.qml \
    aurora_4_0/ofvpn/*.qml

OSVERSION = $$system(cat /etc/issue | cut -d\' \' -f2)
VERSIONS = $$split(OSVERSION, ".")
VERSION_MAJ = $$member(VERSIONS, 0)
VERSION_MIN = $$member(VERSIONS, 1)
VERSION_PCH = $$member(VERSIONS, 2)
VERSION_BLD = $$member(VERSIONS, 3)

lessThan(VERSION_MAJ, 4) {
    jolla_settings_files.files = aurora_3_5/ofvpn
    jolla_settings_files.path = /usr/share/sailfish-vpn/
} else {
    jolla_settings_files.files = aurora_4_0/ofvpn
    jolla_settings_files.path = /usr/share/sailfish-vpn/
}
INSTALLS += jolla_settings_files
