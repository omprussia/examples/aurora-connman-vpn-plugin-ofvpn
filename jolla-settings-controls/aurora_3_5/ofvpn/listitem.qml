// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0
import ru.omprussia.ofvpn 1.0

VpnTypeItem {
    objectName: "vpnTypeItem"
    vpnType: "ofvpn"

    //% "OFVPN"
    name: qsTrId("ofvpn-type_name")
    //% "Full featured implementation of the Fortinet PPP+SSL VPN solution"
    description: qsTrId("ofvpn-type_description")
}
