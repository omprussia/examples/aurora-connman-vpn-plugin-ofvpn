// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0
import ru.omprussia.ofvpn 1.0

VpnPlatformEditDialog {
                           //% "Add new OFVPN connection"
    title: newConnection ? qsTrId("ofvpn-new_title")
                           //% "Edit OFVPN connection"
                         : qsTrId("ofvpn-edit_title")

    objectName: "editPage"
    vpnType: "ofvpn"
    firstAdditionalItem: ofvpnPort

    Component.onCompleted: {
        init();

        ofvpnPort.text = getProviderProperty('ofvpn.Port');
        ofvpnUsername.text = getProviderProperty('ofvpn.User');
        ofvpnPassword.text = getProviderProperty('ofvpn.Password');
        ofvpnAskOTP.checked = getProviderProperty('ofvpn.AskOTP') === 'true';
    }

    onAccepted: {
        updateProvider('ofvpn.Port', ofvpnPort.text);
        updateProvider('ofvpn.User', ofvpnUsername.text);
        updateProvider('ofvpn.Password', ofvpnPassword.text);
        updateProvider('ofvpn.AskOTP', ofvpnAskOTP.checked ? 'true' : 'false');

        saveConnection();
    }

    ConfigTextField {
        id: ofvpnPort

        objectName: "ofvpnPort"
        inputMethodHints: Qt.ImhDigitsOnly

        //% "Port"
        label: qsTrId("ofvpn-server_port")
    }

    SectionHeader {
        objectName: "authentication"
        //% "Authentication"
        text: qsTrId("ofvpn-authentication")
    }

    ConfigTextField {
        id: ofvpnUsername

        objectName: "ofvpnUsername"
        //% "Username"
        label: qsTrId("ofvpn-user_name")
    }

    ConfigPasswordField {
        id: ofvpnPassword

        objectName: "ofvpnPassword"
    }

    TextSwitch {
        id: ofvpnAskOTP

        objectName: "ofvpnAskOTP"
        //% "Two-factor authentication"
        text: qsTrId("ofvpn-two_factor_authentication")
    }
}
