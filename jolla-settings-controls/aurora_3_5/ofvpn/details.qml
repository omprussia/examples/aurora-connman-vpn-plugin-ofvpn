// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0

VpnPlatformDetailsPage {
    objectName: "detailsPage"
    //% "OFVPN"
    subtitle: qsTrId("ofvpn-type_name")
}
