// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0

Column {
    function setProperties(providerProperties) {
        ofvpnAllowSelfSignedCert.checked = getProperty('ofvpn.AllowSelfSignedCert') === 'true';
        ofvpnCertSkipCheck.checked = getProperty('ofvpn.CertSkipCheck') === 'true';
        ofvpnSetRoutes.checked = getProperty('ofvpn.SetRoutes') === 'true';
        ofvpnSetDNS.checked = getProperty('ofvpn.SetDNS') === 'true';
        ofvpnHalfInternetRoutes.checked = getProperty('ofvpn.HalfInternetRoutes') === 'true';
        ofvpnInsecureSSL.checked = getProperty('ofvpn.InsecureSSL') === 'true';
        ofvpnTrustedCert.text = getProperty('ofvpn.TrustedCert');
        ofvpnUserCert.path = getProperty('ofvpn.UserCert');
        ofvpnUserKey.path = getProperty('ofvpn.UserKey');
        ofvpnCAfile.path = getProperty('ofvpn.CAfile');
    }

    function updateProperties(providerProperties) {
        updateProvider('ofvpn.AllowSelfSignedCert', ofvpnAllowSelfSignedCert.checked ? 'true' : 'false');
        updateProvider('ofvpn.CertSkipCheck', ofvpnCertSkipCheck.checked ? 'true' : 'false');
        updateProvider('ofvpn.SetRoutes', ofvpnSetRoutes.checked ? 'true' : 'false');
        updateProvider('ofvpn.SetDNS', ofvpnSetDNS.checked ? 'true' : 'false');
        updateProvider('ofvpn.HalfInternetRoutes', ofvpnHalfInternetRoutes.checked ? 'true' : 'false');
        updateProvider('ofvpn.InsecureSSL', ofvpnInsecureSSL.checked ? 'true' : 'false');
        updateProvider('ofvpn.TrustedCert', ofvpnTrustedCert.text);
        updateProvider('ofvpn.UserCert', ofvpnUserCert.path);
        updateProvider('ofvpn.UserKey', ofvpnUserKey.path);
        updateProvider('ofvpn.CAfile', ofvpnCAfile.path);
    }

    objectName: "content"
    width: parent.width

    SectionHeader {
        objectName: "connection"
        //% "Connection"
        text: qsTrId("ofvpn-connection")
    }

    TextSwitch {
        id: ofvpnSetDNS

        objectName: "ofvpnSetDNS"
        //% "Add VPN nameservers in /etc/resolv.conf when tunnel is up"
        text: qsTrId("ofvpn-set_dns")
    }

    TextSwitch {
        id: ofvpnSetRoutes

        objectName: "ofvpnSetRoutes"
        //% "Set if openfortivpn should try to configure IP routes through the VPN when tunnel is up"
        text: qsTrId("ofvpn-try_to_configure_ip_routes")
    }

    TextSwitch {
        id: ofvpnHalfInternetRoutes

        objectName: "ofvpnHalfInternetRoutes"
        //% "Add 0.0.0.0/1 and 128.0.0.0/1 routes with higher priority"
        text: qsTrId("ofvpn-half_internet_routs")
    }

    TextSwitch {
        id: ofvpnInsecureSSL

        objectName: "ofvpnInsecureSSL"
        //% "Do not disable insecure SSL protocols/ciphers"
        text: qsTrId("ofvpn-insecure_ssl")
    }

    SectionHeader {
        objectName: "certificates"
        //% "Certificates"
        text: qsTrId("ofvpn-certificates")
    }

    TextSwitch {
        id: ofvpnCertSkipCheck

        objectName: "ofvpnCertSkipCheck"
        //% "Skip certificate check"
        text: qsTrId("ofvpn-skip_certificate_check")
    }

    TextSwitch {
        id: ofvpnAllowSelfSignedCert

        objectName: "ofvpnAllowSelfSignedCert"
        //% "Allow self signed certificate"
        text: qsTrId("ofvpn-allow_self_signed_certificate")
    }

    ConfigTextField {
        id: ofvpnTrustedCert

        objectName: "ofvpnTrustedCert"
        //% "Trusted certificate"
        label: qsTrId("ofvpn-trusted_cert")
    }

    ConfigPathField {
        id: ofvpnUserCert

        objectName: "ofvpnUserCert"
        //% "User certificate"
        label: qsTrId("ofvpn-user_certificate")
    }

    ConfigPathField {
        id: ofvpnUserKey

        objectName: "ofvpnUserKey"
        //% "User key"
        label: qsTrId("ofvpn-user_key")
    }

    ConfigPathField {
        id: ofvpnCAfile

        objectName: "ofvpnCAfile"
        //% "CA file"
        label: qsTrId("ofvpn-ca_file")
    }
}
